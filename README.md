
# GruvBox dark color scheme for SAIA PG5 Fupla editor

this is a custom color scheme for SAIA PG5 Fupla editor based on the famous `GruvBox dark` color scheme

## installation

copy the file `SFUP52.saiaopt` to your local AppData directory or replace just the section `[ColorScheme:Custom Colors 1]` in your existing file  
usualy the location is "C:\Users\\**[username]**\AppData\Local\SBC\PG5 V2.3.1xx\SFUP52.saiaopt" the PG5 version might be different  
:information_source:
type in `%localappdata%` in a explorer window to get faster to your local AppData directory

## preview GruvBox dark color scheme for SAIA PG5 Fupla editor

![GruvBox dark preview](./SFUP52.saiaopt.png)
